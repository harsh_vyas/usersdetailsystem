<?php
  include '../connection.php';

  if(isset($_SESSION['username']) && !empty($_SESSION['username']) )
      {
        echo 'Welcome, '. $_SESSION['username'];
      }
?>

<form action="admin-login.php" method="POST">
     <input class="btn gradient-bg text-right" type="submit" name="logout" value="logout">
</form>

<!DOCTYPE html>
<html>
  <head>
  	<title>Home</title>
  	<link rel="stylesheet" type="text/css" href="style.css">
  	<style>
  			table, td, th 
        {
  		     border: 1px solid black;
  		  }

  		  table 
        {
    		   width: 100%;
    		   border-collapse: collapse;
  		  }

  	 	.topnav 
      {
    
        width: 100%;
        overflow: auto;
        background-color: #1f1f2e;
        text-align: center;

      }


      .topnav a 
      {
    
        margin: 0px 50px;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 22px;
        width: 25%;
        text-align: center;
      }

     .topnav a:hover 
      {
        background-color: #ddd;
        color: black;

      }

    .topnav a.active
      {
        background-color: #04AA6D;
        color: white;

      }

    @media screen and (max-width: 500px)
     {
      .topnav a
       {
          float: none;
          display: block;
          width: 100%;
          text-align: left;
      }
  }
  	</style>
</head>
  	<body>
  	<div class="header">
  		<h2>Home Page</h2>
  	</div>
  	<nav class="topnav" id="tapannav">	
  	<a  href='admin-index.php'>Home</a> |
  	<a  href='admin-approve.php'>New Users</a> |

  	</nav>
  	<br><br><br>
  	<div class="content">
  		<table >
                 
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th> 
                    <th>Password</th>
                    <th>Phone</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                  
                 
                  <?php
                  
                  $sql =mysqli_query($conn,"select * from user");
                  while($row= mysqli_fetch_array($sql,MYSQLI_ASSOC))
                  {
                  ?>
                  <tr>
                    <td><?php echo $row['id']; ?></td>
                    <td><?php echo $row['username']; ?></td>
                    <td><?php echo $row['email']; ?></td>
                    <td><?php echo $row['Address']; ?></td>
                    <td><?php echo $row['password']; ?></td>
                    <td><?php echo $row['number']; ?></td>
                    <td><?php echo $row['Date']; ?></td>                  
                    <td><?php echo $row['status']; ?></td>
                    <td><a href="edit.php?id=<?php echo $row['id']; ?>">Edit</a></td>
                    <td><a href="delete.php?id=<?php echo $row['id']; ?>">Delete</a></td>
                    
                  </tr>
                  <?php
                  }
                  ?>
              </table>            	  		
  	</body>
</html>
<? php
if(isset($_POST['Delete']))
            {
                $id = $_POST['Delete'];
                $query="delete from user where id='$id'";
                
            if(mysqli_query($conn, $query))
            {
              
            }
            else
            {
                echo("Error description: " . mysqli_error($conn));
            }
            }
?>
